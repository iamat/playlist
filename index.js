/**
 * Playlist module
 **/

var inherits = require("inherits"),
    EventEmitter = require("eventemitter2").EventEmitter2;

var playlist = module.exports = function (pl) {
    EventEmitter.call(this);

    this._init(pl);
};

inherits(playlist, EventEmitter);

playlist.prototype._init = function (pl) {
    var self = this;

    self.playlist = [];
    self.startTime = 0;
    self._timeouts = [];
    self.playedTime = 0;
    self.lastModified = 0;
    self.name = "";

    if (pl) {
        self.playlist = pl.playlist;
        self.startTime = pl.startTime || self.startTime;
        self.playing = pl.playing || false;
        self.name = pl.name || self.name;
        self.lastModified = pl.lastModified || self.lastModified;
        self.playedTime = pl.playedTime || self.playedTime;
    }

    if (self.playing) {
        self.play();
    }
};

playlist.prototype.play = function () {
    var self = this,
        now = Date.now(),
        baseTime = 0,
        missed = 0;
    self.startTime = self.startTime || now;
    self.playedTime = self.playedTime || 0;
    self.lastModified = self.lastModified || now;

    if (self.playing) {
        missed = now - self.lastModified;
        if (self.startTime < now) {
            self.playedTime += missed;
        }
        self.lastModified = now;
    }

    self.playing = true;

    if (!self.playedTime) {
        baseTime = self.startTime - now;
    } else {
        baseTime = -1* self.playedTime;
    }

    self.playlist.forEach(function (item) {
        var time = baseTime + item.time;
        if (time < 0 && (time + missed) > 0) {
            time = 0;
        }
        if (item.disabled || time < 0) {
            return;
        }
        self._timeouts.push(setTimeout(function () {
            self.playedTime = item.time;
            self.lastModified = Date.now();
            self.emit(item.event, item.data);
            self.emit("new_item", item);
        }, time));
    });
};

playlist.prototype.pause = function () {
    var self = this;

    var now = Date.now();

    if (self.playing) {
        self.playedTime += now - self.lastModified;
    }

    self.lastModified = now;
    self._timeouts.forEach(function (id) {
        clearTimeout(id);
    });
    self.playing = false;
};

playlist.prototype._getNextItem = function () {
    var self = this;

    var results = [],
        closest = Infinity;

    self.playlist.forEach(function (item) {
        var diff = item.time - self.playedTime;

        if (diff < 0 || item.disabled) { return; }
        if (diff === closest) {
            results.push(item);
        } else if (diff < closest) {
            results = [item];
            closest = diff;
        }
    });

    return results;
};

playlist.prototype.next = function () {
    var self = this;

    var nextItems = [],
        wasPlaying = self.playing;

    self.pause();
    nextItems = self._getNextItem(),

    nextItems.forEach(function (item) {
        self.emit(item.event, item.data);
        self.playedTime = item.time + 1;
    });

    self.lastModified = Date.now();
    if (wasPlaying) { self.play(); }
};

playlist.prototype.seek = function (time) {
    var self = this,
        wasPlaying = self.playing;

    if (wasPlaying) { self.pause(); }

    self.playedTime = time;
    self.lastModified = Date.now();

    if (wasPlaying) { self.play(); }
};

playlist.prototype.update = function (pl) {
    var self = this,
        key = "";

    self.stop();

    self._init(pl);
};

playlist.prototype.getState = function () {
    var self = this;

    var now = Date.now();

    if (self.playing) {
        self.playedTime += now - self.lastModified;
    }

    self.lastModified = now;

    return {
        name: self.name,
        startTime: self.startTime,
        playlist: self.playlist,
        lastModified: self.lastModified,
        playedTime: self.playedTime,
        playing: self.playing
    };
};

playlist.prototype.stop = function () {
    var self = this;

    self.pause();
    self.lastModified = Date.now();
    self.playedTime = 0;
};
