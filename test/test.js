/**
 * Tests for playlist module
 */

var assert = require("chai").assert,
    expect = require("chai").expect,
    sinon = require("sinon");

var clone = require("clone");

var Playlist = require("../index.js");


var stoppedPlaylist = { name: "test",
                        startTime: 0,
                        playlist: [
                            {
                                event: "test0",
                                data: { foo: "bar-0" },
                                time: 0
                            }, {
                                event: "test1",
                                data: { foo: "bar-2" },
                                time: 10
                            }, {
                                event: "test2",
                                data: { foo: "bar-2" },
                                time: 20
                            }, {
                                event: "test3",
                                data: { foo: "bar-3" },
                                time: 30
                            }, {
                                event: "test0",
                                data: { foo: "bar-4" },
                                time: 40
                            }
                        ]
                      },
    unorderedPlaylist = { name: "test",
                        startTime: 0,
                        playlist: [
                            {
                                event: "test3",
                                data: { foo: "bar-3" },
                                time: 30
                            }, {
                                event: "test1",
                                data: { foo: "bar-2" },
                                time: 10
                            }, {
                                event: "test2",
                                data: { foo: "bar-2" },
                                time: 20
                            }, {
                                event: "test0",
                                data: { foo: "bar-0" },
                                time: 0
                            }, {
                                event: "test0",
                                data: { foo: "bar-4" },
                                time: 40
                            }
                        ]
                      };

suite("init", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("has basic methods", function () {
        var pl = new Playlist();
        expect(pl).to.be.instanceOf(Playlist);
        expect(pl).to.respondTo("on");
        expect(pl).to.respondTo("play");
        expect(pl).to.respondTo("next");
        expect(pl).to.respondTo("seek");
        expect(pl).to.respondTo("update");
        expect(pl).to.respondTo("pause");
        expect(pl).to.respondTo("stop");
    });

    test("autoplay with startTime now", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = Date.now();
        template.playing = true;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        clock.tick(5000);

        expect(results).to.have.length(5);

        results.forEach(function (item, i) {
            expect(item).to.have.property("event", template.playlist[i].event);
            expect(item).to.have.property("data")
                .that.deep.equals(template.playlist[i].data);
        });
    });

    test("autoplay with startTime 0", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = 0;
        template.playing = true;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        clock.tick(5000);

        expect(results).to.have.length(5);

        results.forEach(function (item, i) {
            expect(item).to.have.property("event", template.playlist[i].event);
            expect(item).to.have.property("data")
                .that.deep.equals(template.playlist[i].data);
        });
    });

    test("autoplay with seeked playlist", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        template.startTime = Date.now() - 500;
        template.lastModified = Date.now() - 5;
        template.playedTime = 14;
        template.playing = true;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        expect(results).to.have.length(0); // pt: 19
        expect(pl.getState()).to.have.property("playedTime", 19);

        clock.tick(2); // pt: 21
        expect(results).to.have.length(1);
        expect(results).to.include("test2");

        clock.tick(10); // pt; 31
        expect(results).to.have.length(2);
        expect(results).to.include("test3");

        clock.tick(5000); // pt: 5031
        expect(results).to.have.length(3);
    });

    test("stopped", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = Date.now();
        template.play = false;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        clock.tick(5000);

        expect(results).to.have.length(0);
    });
});

suite("#play", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("works with default values", function () {
        var pl = new Playlist();

        pl.play();
        expect(pl).to.have.property("startTime", Date.now());
        expect(pl).to.have.property("lastModified", Date.now());
        expect(pl).to.have.property("playedTime", 0);
    })

    test("sets inital values", function () {
        var pl = new Playlist(stoppedPlaylist);

        pl.play();
        expect(pl).to.have.property("startTime", Date.now());
        expect(pl).to.have.property("lastModified", Date.now());
        expect(pl).to.have.property("playedTime", 0);
    });

    test("startTime 0", function () {
        var pl = new Playlist(stoppedPlaylist),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();

        clock.tick(1);
        expect(results).to.have.length(1);
        expect(results[0])
            .to.have.property("event", stoppedPlaylist.playlist[0].event);
        expect(results[0]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[0].data);

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results[1])
            .to.have.property("event", stoppedPlaylist.playlist[1].event);
        expect(results[1]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[1].data);

        clock.tick(10);
        expect(results).to.have.length(3);
        expect(results[2])
            .to.have.property("event", stoppedPlaylist.playlist[2].event);
        expect(results[2]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[2].data);

        clock.tick(10);
        expect(results).to.have.length(4);
        expect(results[3])
            .to.have.property("event", stoppedPlaylist.playlist[3].event);
        expect(results[3]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[3].data);

        clock.tick(10);
        expect(results).to.have.length(5);
        expect(results[4])
            .to.have.property("event", stoppedPlaylist.playlist[4].event);
        expect(results[4]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[4].data);
    });

    test("startTime in future", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = Date.now() + 50;
        template.lastModified = Date.now() - 10;
        template.playedTime = 0;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();

        clock.tick(49);
        expect(results).to.have.length(0);

        clock.tick(2);
        expect(results).to.have.length(1);
        expect(results[0])
            .to.have.property("event", stoppedPlaylist.playlist[0].event);
        expect(results[0]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[0].data);

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results[1])
            .to.have.property("event", stoppedPlaylist.playlist[1].event);
        expect(results[1]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[1].data);

        clock.tick(10);
        expect(results).to.have.length(3);
        expect(results[2])
            .to.have.property("event", stoppedPlaylist.playlist[2].event);
        expect(results[2]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[2].data);

        clock.tick(10);
        expect(results).to.have.length(4);
        expect(results[3])
            .to.have.property("event", stoppedPlaylist.playlist[3].event);
        expect(results[3]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[3].data);

        clock.tick(10);
        expect(results).to.have.length(5);
        expect(results[4])
            .to.have.property("event", stoppedPlaylist.playlist[4].event);
        expect(results[4]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[4].data);
    });

    test("unordered playlist", function () {
        var template = clone(unorderedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = Date.now() + 50;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();

        clock.tick(49);
        expect(results).to.have.length(0);

        clock.tick(2);
        expect(results).to.have.length(1);
        expect(results[0])
            .to.have.property("event", stoppedPlaylist.playlist[0].event);
        expect(results[0]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[0].data);

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results[1])
            .to.have.property("event", stoppedPlaylist.playlist[1].event);
        expect(results[1]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[1].data);

        clock.tick(10);
        expect(results).to.have.length(3);
        expect(results[2])
            .to.have.property("event", stoppedPlaylist.playlist[2].event);
        expect(results[2]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[2].data);

        clock.tick(10);
        expect(results).to.have.length(4);
        expect(results[3])
            .to.have.property("event", stoppedPlaylist.playlist[3].event);
        expect(results[3]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[3].data);

        clock.tick(10);
        expect(results).to.have.length(5);
        expect(results[4])
            .to.have.property("event", stoppedPlaylist.playlist[4].event);
        expect(results[4]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[4].data);
    });

    test("startTime in past", function () {
        var template = clone(stoppedPlaylist),
            pl,
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push({event: event, data: data});
            };
        };

        template.startTime = Date.now() - 30;
        pl = new Playlist(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();

        clock.tick(1);
        expect(results).to.have.length(1);
        expect(results[0])
            .to.have.property("event", stoppedPlaylist.playlist[3].event);
        expect(results[0]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[3].data);

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results[1])
            .to.have.property("event", stoppedPlaylist.playlist[4].event);
        expect(results[1]).to.have.property("data")
            .that.deep.equals(stoppedPlaylist.playlist[4].data);
    });

    test("skip disabled elements", function () {
        var template = clone(stoppedPlaylist),
            pl;

        var spy = sinon.spy();

        template.startTime = Date.now() - 21;
        template.playlist[0].disabled = true;
        template.playlist[4].disabled = true;

        pl = new Playlist(template);

        pl.on("test0", spy);

        pl.play();

        clock.tick(5000);

        expect(spy.callCount).to.equal(0);
    });

    test("continue paused", function () {
        var template = clone(stoppedPlaylist),
            pl;

        var spy = sinon.spy();

        pl = new Playlist(template);

        pl.on("test0", spy);
        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        pl.play();

        clock.tick(25);
        expect(spy.callCount).to.equal(3);
        pl.pause();

        clock.tick(5000);
        expect(spy.callCount).to.equal(3);
        pl.play();

        clock.tick(6);
        expect(spy.callCount).to.equal(4);
    });
});

suite("#on('new_item')", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("startTime 0", function () {
        var pl = new Playlist(stoppedPlaylist),
            results = [];

        var spy = function (item) {
            results.push(item);
        };

        pl.on("new_item", spy);

        pl.play();

        clock.tick(1);
        expect(results).to.have.length(1);
        expect(results[0])
              .to.deep.equal(stoppedPlaylist.playlist[0]);

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results[1])
              .to.deep.equal(stoppedPlaylist.playlist[1]);

        clock.tick(10);
        expect(results).to.have.length(3);
        expect(results[2])
            .to.deep.equal(stoppedPlaylist.playlist[2]);

        clock.tick(10);
        expect(results).to.have.length(4);
        expect(results[3])
            .to.deep.equal(stoppedPlaylist.playlist[3]);

        clock.tick(10);
        expect(results).to.have.length(5);
        expect(results[4])
            .to.deep.equal(stoppedPlaylist.playlist[4]);
    });
})

suite("#pause", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("playedTime is set right", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template);

        pl.play();

        clock.tick(25);
        pl.pause();

        expect(pl).to.have.property("playedTime", 25);
        expect(pl).to.have.property("lastModified", Date.now());

        clock.tick(25);
        expect(pl).to.have.property("playedTime", 25);
        expect(pl).to.have.property("lastModified", Date.now() - 25);
        pl.play();

        clock.tick(10);
        pl.pause();
        expect(pl).to.have.property("playedTime", 35);
        expect(pl).to.have.property("lastModified", Date.now());
    });

    test("no further elements are played", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            spy = sinon.spy();

        pl.on("test2", spy);
        pl.on("test3", spy);

        pl.play();

        clock.tick(15);
        pl.pause();

        clock.tick(5000);
        expect(spy.callCount).to.equal(0);
    });
});

suite("#next", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("fires next element", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.next();
        expect(results).to.have.length(1);
        expect(results[0]).to.equal("test0");

        pl.next();
        expect(results).to.have.length(2);
        expect(results[1]).to.equal("test1");

        pl.next();
        expect(results).to.have.length(3);
        expect(results[2]).to.equal("test2");

        pl.next();
        expect(results).to.have.length(4);
        expect(results[3]).to.equal("test3");

        pl.next();
        expect(results).to.have.length(5);
        expect(results[0]).to.equal("test0");
    });

    test("fires next element", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.next();
        expect(results).to.have.length(1);
        expect(results[0]).to.equal("test0");

        pl.next();
        expect(results).to.have.length(2);
        expect(results[1]).to.equal("test1");

        pl.next();
        expect(results).to.have.length(3);
        expect(results[2]).to.equal("test2");

        pl.next();
        expect(results).to.have.length(4);
        expect(results[3]).to.equal("test3");

        pl.next();
        expect(results).to.have.length(5);
        expect(results[0]).to.equal("test0");
    });

    test("ignores disabled element", function () {
        var template = clone(stoppedPlaylist),
            pl,
            spy = sinon.spy();

        template.playlist[1].disabled = true;
        pl = new Playlist(template),

        pl.on("test1", spy);

        pl.next();
        pl.next();
        pl.next();
        pl.next();

        expect(spy.callCount).to.equal(0);
    });

    test("next on last element wont change state", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            spy = sinon.spy();

        pl.on("test0", spy);
        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        pl.next();
        pl.next();
        pl.next();
        pl.next();
        pl.next();
        expect(spy.callCount).to.equal(5);
        expect(pl.getState()).to.have.property("playedTime", 41);
        pl.next();
        expect(spy.callCount).to.equal(5);
        expect(pl.getState()).to.have.property("playedTime", 41);
        pl.next();
        expect(spy.callCount).to.equal(5);
        expect(pl.getState()).to.have.property("playedTime", 41);
    });

    test("keeps on playing", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();
        clock.tick(5); // rt: 5ms, pt: 5ms

        pl.next(); // rt: 5ms, pt: 11ms
        expect(results).to.have.length(2);
        expect(results[1]).to.equal("test1");

        clock.tick(8); // rt: 13ms, pt: 19ms
        expect(results).to.have.length(2);

        clock.tick(2); // rt: 15ms, pt: 21ms
        expect(results).to.have.length(3);
        expect(results[2]).to.equal("test2");

        pl.next(); // rt: 15ms, pt: 31ms
        expect(results).to.have.length(4);
        expect(results[3]).to.equal("test3");

        clock.tick(8); // rt: 23ms, pt: 39ms
        expect(results).to.have.length(4);

        clock.tick(2); // rt: 25ms, pt: 41ms
        expect(results).to.have.length(5);
        expect(results[0]).to.equal("test0");
    });

    test("stays stopped", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.next();
        expect(results).to.have.length(1);
        expect(results[0]).to.equal("test0");

        clock.tick(100);
        expect(results).to.have.length(1);

        pl.next();
        expect(results).to.have.length(2);
        expect(results[1]).to.equal("test1");

        clock.tick(100);
        expect(results).to.have.length(2);
    });
});

suite("#seek", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("seeks to the right time", function () {});
    test("keeps on playing", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();
        clock.tick(5); // p: 5ms, r: 5ms

        expect(results).to.have.length(1);
        pl.seek(15); // p: 15ms, r: 5ms

        clock.tick(100);
        expect(results).to.have.length.above(1);

    });
    test("stays paused", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();
        clock.tick(5); // p: 5ms, r: 5ms

        pl.pause();
        expect(results).to.have.length(1);
        pl.seek(15); // p: 15ms, r: 5ms

        clock.tick(100);
        expect(results).to.have.length(1);
    });
    test("seek to future doesn't call elements in between", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();
        clock.tick(5); // p: 5ms, r: 5ms

        pl.seek(15); // p: 15ms, r: 5ms
        clock.tick(4); // p: 19ms, r: 9ms
        expect(results).to.have.length(1);
        expect(results[0]).to.equal("test0");

        clock.tick(2); // p: 21ms, r: 11ms
        expect(results).to.have.length(2);
        expect(results[1]).to.equal("test2");

        clock.tick(10); // p: 31ms, r: 21ms
        expect(results).to.have.length(3);
        expect(results[2]).to.equal("test3");
    });
    test("seek to past playes elements again", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.play();
        clock.tick(11); // p: 11ms, r: 11ms

        pl.seek(5); // p: 5ms, r: 11ms
        clock.tick(6); // p: 11ms, r: 17ms
        expect(results).to.deep.equal(["test0", "test1", "test1"]);

        clock.tick(10); // p: 21ms, r: 27ms
        expect(results).to.deep.equal(["test0", "test1", "test1", "test2"]);
    });
});

suite("#stop", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("no more elements will be played", function () {
        var template = clone(stoppedPlaylist),
            pl;

        var spy = sinon.spy();

        pl = new Playlist(template);

        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        pl.play();
        clock.tick(5);

        pl.stop();
        clock.tick(5000);

        expect(spy.callCount).to.equal(0);
    });
    test("playedTime is 0", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template);

        pl.play();
        clock.tick(10);

        pl.stop();
        expect(pl).to.have.property("playedTime", 0);
    });
});

suite("#getState", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("initial state", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template);

        expect(pl.getState()).to.have.property("name", stoppedPlaylist.name);
        expect(pl.getState()).to.have.property("startTime", stoppedPlaylist.startTime);
        expect(pl.getState()).to.have.property("playlist")
            .that.deep.equals(stoppedPlaylist.playlist);
        expect(pl.getState()).to.have.property("playing", false);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 0);
    });

    test("while playlist is running", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template);

        pl.play();
        expect(pl.getState()).to.have.property("playing", true);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 0);

        clock.tick(5);
        expect(pl.getState()).to.have.property("playing", true);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 5);

        clock.tick(15);
        expect(pl.getState()).to.have.property("playing", true);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 20);

        clock.tick(15);
        pl.pause();
        expect(pl.getState()).to.have.property("playing", false);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 35);

        clock.tick(5);
        expect(pl.getState()).to.have.property("playing", false);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", 35);
    });
});

suite("#update", function () {
    var clock;
    setup(function () {
        clock = sinon.useFakeTimers(Date.now());
    });
    teardown(function () {
        clock.restore();
    });

    test("correct state", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            before;

        before = pl.getState();

        pl.play();
        clock.tick(35);

        pl.update(template);
        expect(pl.getState()).to.have.property("name", before.name);
        expect(pl.getState()).to.have.property("startTime", before.startTime);
        expect(pl.getState()).to.have.property("playlist")
            .that.deep.equals(before.playlist);
        expect(pl.getState()).to.have.property("playing", before.playing);
        expect(pl.getState()).to.have.property("lastModified", Date.now());
        expect(pl.getState()).to.have.property("playedTime", before.playedTime);
    });

    test("maintaines listeners", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            before;

        before = pl.getState();

        pl.play();
        clock.tick(35);

        pl.onAny(function () {});
        expect(pl.listenersAny()).to.have.length(1);
        pl.update(template);
        expect(pl.listenersAny()).to.have.length(1);
    });


    test("stopped", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            spy = sinon.spy();

        pl.play();
        clock.tick(35);
        pl.update(template);

        pl.on("test0", spy);
        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        clock.tick(50000);
        expect(spy.callCount).to.equal(0);
    });

    test("playing", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            spy = sinon.spy();

        pl.play();
        clock.tick(35);
        template.playing = true;
        pl.update(template);

        pl.on("test0", spy);
        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        clock.tick(50000);
        expect(spy.callCount).to.equal(5);
    });

    test("playing with start time in the past", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            spy = sinon.spy();

        pl.play();
        clock.tick(35);
        template.playing = true;
        pl.update(template);

        pl.on("test0", spy);
        pl.on("test1", spy);
        pl.on("test2", spy);
        pl.on("test3", spy);

        clock.tick(50000);
        expect(spy.callCount).to.equal(5);
    });

    test("autoplay with seeked playlist", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };
        pl.play();
        clock.tick(35);

        template.startTime = Date.now() - 500;
        template.lastModified = Date.now() - 5;
        template.playedTime = 14;
        template.playing = true;

        pl.update(template);

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        expect(results).to.have.length(0); // pt: 19
        expect(pl.getState()).to.have.property("playedTime", 19);

        clock.tick(2); // pt: 21
        expect(results).to.have.length(1);
        expect(results).to.include("test2");

        clock.tick(10); // pt; 31
        expect(results).to.have.length(2);
        expect(results).to.include("test3");

        clock.tick(5000); // pt: 5031
        expect(results).to.have.length(3);
    });

    test("autoplay with startTime in futue", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };
        pl.play();
        clock.tick(35);

        template.playing = true;
        template.startTime = Date.now() + 50;
        template.lastModified = Date.now() - 10;
        template.playedTime = 0;

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.update(template);

        clock.tick(49);
        expect(results).to.have.length(0);

        clock.tick(2);
        expect(results).to.have.length(1);
        expect(results).to.include("test0");

        clock.tick(10);
        expect(results).to.have.length(2);
        expect(results).to.include("test1");
    });

    test("autoplay with missed events between last modified and now", function () {
        var template = clone(stoppedPlaylist),
            pl = new Playlist(template),
            results = [];

        var spy = function (event) {
            return function (data) {
                results.push(event);
            };
        };
        pl.play();
        clock.tick(35);

        template.playing = true;
        template.startTime = Date.now() - 20;
        template.lastModified = Date.now() - 10;
        template.playedTime = 15;

        pl.on("test0", spy("test0"));
        pl.on("test1", spy("test1"));
        pl.on("test2", spy("test2"));
        pl.on("test3", spy("test3"));

        pl.update(template);
        expect(pl).to.have.property("playedTime", 25);

        clock.tick(1); // pt: 26
        expect(results).to.have.length(1);
        expect(results).to.include("test2");

        clock.tick(5);
        expect(results).to.have.length(2);
        expect(results).to.include("test3");
    });
});
